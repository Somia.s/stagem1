#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import plotly.figure_factory as ff




########################################################################################################################
###	Search shorter distances on the left, on the center and, on the right 
### between each genes and all occurences of one TE family
########################################################################################################################


def calculateDistances_Genes_TEs(TEname, dataFrame_Genes, dataFrame_MyTE, dataFrame_Overlap):
    
    # list of data structure
    allDistances = []
        
    len_dfGenes = len(dataFrame_Genes)
    
    id_TE = 0
    while id_TE < len(dataFrame_MyTE): # for each TE occurence
        id_gene = 0
        
        # This two overlap a TFBS ? Colum answers by 'yes' or 'no'
        overlapTE = "No"
        
        # chromosome of TE
        chr_TE = dataFrame_MyTE['Chr'][id_TE]
        chr_TE_2 = chr_TE.replace('chr','')
        
        # start of TE
        start_TE = dataFrame_MyTE['Start'][id_TE]
        # end of TE
        end_TE = dataFrame_MyTE['End'][id_TE]
        # strand of TE
        strand_TE = dataFrame_MyTE['Sens'][id_TE]
        
        ## Initialize variables
        # Shorter Distances between gene and TE
        shorterDistanceLeft_kept = None
        shorterDistanceRight_kept = None
        shorter_distance = None # shorter distance between left and right
        firstValue_left = True
        firstValue_right = True
        # Gene Name
        gene_left = None
        gene_right = None
        gene_center = None
        # Gene Strand
        gene_left_strand = None
        gene_right_strand = None
        gene_center_strand = None
        # Gene Function
        gene_left_function = None
        gene_right_function = None
        gene_center_function = None
        
        # This TE has overlapped a TFBS ?
        len_df_overlap = len(dataFrame_Overlap)
        idTE_overlap = 0
        while idTE_overlap < len_df_overlap:
            if int(start_TE) == int(dataFrame_Overlap['Start TE'][idTE_overlap]) and int(end_TE) == int(dataFrame_Overlap['End TE'][idTE_overlap]):
                overlapTE = "Yes"
            idTE_overlap+=1            
            
          
        while id_gene < len_dfGenes: # compare each gene with all occurences of one TE family
            
            
            # Retrieve informations of this current gene
            #gene_name = dataFrame_Genes['Gene Name'][id_gene] # gene name
            chr_gene = dataFrame_Genes['Chr Name'][id_gene] # chromosome
            start_gene = dataFrame_Genes['Start'][id_gene] # start of gene
            end_gene = dataFrame_Genes['End'][id_gene] # end of gene         
            
            if chr_TE_2 == chr_gene: # select only occurences TE on same chromosome of this current gene
                    
                ############### Two cases ###############
                
                ####### 1. On the left side of this current TE #######
                if end_gene < start_TE:
                    # Initialization of distance value with first case
                    if firstValue_left == True :
                        shorterDistanceLeft_kept = int(start_TE - end_gene)
                        firstValue_left = False # this is not first value anymore
                        gene_left = dataFrame_Genes['Gene Name'][id_gene]
                        gene_left_strand = dataFrame_Genes['Sens'][id_gene]
                        gene_left_function = dataFrame_Genes['Function'][id_gene]
                    
                    # distance between current TE and the closest gene
                    shorterDistanceLeft = int(start_TE - end_gene)
                    
                    # compare to kept shorter distance, keep current value if it is shorter
                    # get rid of the greatest value
                    if shorterDistanceLeft < shorterDistanceLeft_kept:
                        shorterDistanceLeft_kept = shorterDistanceLeft
                        gene_left = dataFrame_Genes['Gene Name'][id_gene]
                        gene_left_strand = dataFrame_Genes['Sens'][id_gene]
                        gene_left_function = dataFrame_Genes['Function'][id_gene]
                        


                
                ####### 2. On right side of this current TE #######
                elif start_gene > end_TE:
                    # Initialization of distance value with first case
                    if firstValue_right == True:
                        shorterDistanceRight_kept = int(start_gene - end_TE)
                        firstValue_right = False # this is not first value anymore
                        gene_right = dataFrame_Genes['Gene Name'][id_gene]
                        gene_right_strand = dataFrame_Genes['Sens'][id_gene]
                        gene_right_function = dataFrame_Genes['Function'][id_gene]
                    
                    # distance between current TE and the closest gene
                    shorterDistanceRight = int(start_gene - end_TE)
                    
                    # compare to Right shorter distance, keep current value if it is shorter
                    if shorterDistanceRight < shorterDistanceRight_kept:
                        shorterDistanceRight_kept = int(shorterDistanceRight)
                        gene_right = dataFrame_Genes['Gene Name'][id_gene]
                        gene_right_strand = dataFrame_Genes['Sens'][id_gene]
                        gene_right_function = dataFrame_Genes['Function'][id_gene]
                        
                
                    
                ####### 3. TE inside gene, or overlap#######
                elif start_TE in list(range(start_gene, end_gene)) and end_TE in list(range(start_gene, end_gene)):
                    gene_center = dataFrame_Genes['Gene Name'][id_gene]
                    gene_center_strand = dataFrame_Genes['Sens'][id_gene]
                    gene_center_function = dataFrame_Genes['Function'][id_gene]
                
            # change occurence gene
            id_gene+=1
            
        # A column with shorter distances TE-gene without regarding on the left side and on the right side, in order to plot histogram of distances
        if shorterDistanceLeft_kept == None:
            shorter_distance = shorterDistanceRight_kept
        elif shorterDistanceRight_kept == None:
            shorter_distance = shorterDistanceLeft_kept
        elif shorterDistanceLeft_kept != None and shorterDistanceRight_kept != None:
            shorter_distance = min(shorterDistanceLeft_kept, shorterDistanceRight_kept)

   
        # assign value to data structure for each gene
        list_distances = [TEname, overlapTE, chr_TE, start_TE, end_TE, strand_TE, shorterDistanceLeft_kept, gene_left, gene_left_strand, gene_left_function, shorterDistanceRight_kept, gene_right, gene_right_strand, gene_right_function, gene_center, gene_center_strand, gene_center_function, shorter_distance]

        allDistances.append(list_distances)
                        
        id_TE+=1
    
    # convert to panda format
    # a line for each TE occurence
    list_distances_all = [x for x in allDistances if x != []]
    dataFrame_distances = pd.DataFrame(list_distances_all, columns=['TE Name', 'Overlap TE/TFBS', 'Chr', 'Start TE', 'End TE', 'Strand TE', 'Shorter Distance Left', 'Gene Name Left', 'Gene Strand Left', 'Gene Function Left', 'Shorter Distance Right', 'Gene Name Right', 'Gene Strand Right', 'Gene Function Right', 'Gene Name Center', 'Gene Strand Center', 'Gene Function Center', 'Shorter Distance'])
    
    return dataFrame_distances




########################################################################################################################
###	Graphics
########################################################################################################################
 
    
def histogramDistances_GenesTEs(df, xlab, title, pathOutputHTML):
    
    # replace values > 1M by 1M
    df[df[xlab] > 1000000] = 1000000
    
    # split values of overlap with TFBS and values without overlap
    masque1 = df['Overlap TE/TFBS'] == 'No'
    data1 = df[masque1]  
    masque2 = df['Overlap TE/TFBS'] == 'Yes'
    data2 = df[masque2]
    
    list_notOverlap = data1[xlab].dropna() # remove NA values
    list_Overlap = data2[xlab].dropna()
    
    max1 = max(list_notOverlap)
    max2 = max(list_Overlap)
    maximum = max(max1, max2)

    interval = maximum/10

    group_labels = ['TE not overlapping a TFBS', 'TE overlapping a TFBS']
    
    # Create distplot with custom bin_size
    fig = ff.create_distplot([list_notOverlap, list_Overlap], group_labels,
                             bin_size=interval)

    # add title
    fig.update_layout(title_text=title)
    # conserve in a HTML file
    fig.write_html(pathOutputHTML)