#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd



########################################################################################################################
###	Integrate Gene Ontology (around overlaps between TE and TFBS)
### Important to notice that a same gene may have several GO
########################################################################################################################




def parseGO(filename):
    
    file = open(filename, 'r')
    file.readline() # delete head
    lines = file.readlines()
    
    lists_GO = []
    
    for line in lines:
        decoupe = line.split(",")
        gene_id = decoupe[0]
        GO_name = decoupe[1]
        type_GO = decoupe[2]
        function_GO = decoupe[3].replace('\n','')
        
        lists_GO.append([gene_id, GO_name, type_GO, function_GO])
    
    # convert to panda format
    allGO = [x for x in lists_GO if x != []]
    dataFrame_GO = pd.DataFrame(allGO, columns=['Gene ID', 'GO Name', 'Type GO', 'Function GO'])

    file.close()
    
    return dataFrame_GO




########################################################################################################################
###	For a given TE, retrieve genes around overlapping
########################################################################################################################

def retrieveGO(dataFrame_overlapping, dataFrame_Gene, dataFrame_GO, dataFrame_TwoShorterDistances):
    
    ''' Four parameters
        1. dataFrame_overlapping: dataframe containing informations of overlap
            Its columns: ['Chr', 'TE_name', 'TFBS_ENCFF', 'TFBS_ENCSR', 'Start_TE', 'End_TE', 'Start_TFBS', 'End_TFBS']       
        
        2. dataFrame_Gene: dataframe containing all genes from NCBI
            Its columns: ['Chr Name', 'Chr ID', 'Start', 'End', 'Sens', 'Type', 'ID', 'Gene Name', 'Function']
        
        3. dataFrame_GO: dataframe containing gene ontology
            index: Gene ID
            Its columns: ['GO Name', 'Type GO', 'Function GO']
        
        4. dataFrame_TwoShorterDistances: dataFrame containing distances where we can found the closest genes of overlaps '''
    
    
    # List of data structure
    genic_environment = []

        
    len_df_overlap = len(dataFrame_overlapping)
    #Browse file in terms of TE occurences having overlapped
    id_TE = 0
    while id_TE < len_df_overlap:
        print(id_TE+1, '/', len_df_overlap)
        


        # chromosome where overlap have occured
        chr_overlap = dataFrame_overlapping["Chr"][id_TE]
        chr_overlap = chr_overlap.replace('chr', '')
        # begin of overlap
        start_overlap = dataFrame_overlapping["Start TE"][id_TE]
        start_overlap = int(start_overlap)
        # end of overlap
        end_overlap = dataFrame_overlapping["End TE"][id_TE]
        end_overlap = int(end_overlap)
        
        number_bp = 0
        # number_bp : How many base pairs I have to look at on both sides ?
        # fetch number_bp specific to this current occurence TE
        len_dfDistances = len(dataFrame_TwoShorterDistances)
        idOccurences=0
        while idOccurences < len_dfDistances:
            if dataFrame_TwoShorterDistances['Start TE'][idOccurences] == start_overlap:
                number_bp1 = dataFrame_TwoShorterDistances['Shorter Distance Left'][idOccurences]
                number_bp2 = dataFrame_TwoShorterDistances['Shorter Distance Right'][idOccurences]
                if str(number_bp1) == 'nan':
                    number_bp = number_bp2
                elif str(number_bp2) == 'nan':
                    number_bp = number_bp1
                elif str(number_bp1) != 'nan' and str(number_bp2) != 'nan':
                    print(number_bp1, number_bp2)
                    number_bp = max(int(number_bp1), int(number_bp2))

            idOccurences+=1
        
        # Compare TE overlapping with coordinates of genes from NCBI
        for id_gene, gene in dataFrame_Gene.iterrows():
            
            # chromosome of gene
            chr_gene = dataFrame_Gene["Chr Name"][id_gene]
            
            ## Look at both sides of the overlap:
            # begin of gene
            start_gene = dataFrame_Gene["Start"][id_gene]
            start_gene = int(start_gene)
            # end of gene
            end_gene = dataFrame_Gene["End"][id_gene]
            end_gene = int(end_gene)
                            
            if chr_gene == chr_overlap:        

                #if (start_gene in list(range(start_overlap - number_bp, start_overlap + number_bp)) and end_gene in list(range(end_overlap - number_bp, end_overlap + number_bp))):                    
                if end_gene in list(range(start_overlap-number_bp-1000, start_overlap-number_bp+1000)) or start_gene in list(range(end_overlap+number_bp-1000, end_overlap+number_bp+1000)):
                    
                    geneID_fromGene = dataFrame_Gene["ID"][id_gene].split(',')[0]
                    
                    atLeastOneGO = True
                    
                    masque = dataFrame_GO['Gene ID'] == geneID_fromGene
                    resultsGO = dataFrame_GO[masque]
                    
                    if resultsGO.empty:
                        atLeastOneGO = False # We found at least one GO for 1 gene
                    else:
                        for id_resultGO in list(resultsGO.index):
                            # Assign value to data structure
                            # 2 columns containing names of TFBS associated (ENCFF and ENCSR) + informations about genes (retrieve all) + informations about GO
                            # merge all into the same list
                            list_fromGO = [dataFrame_GO['GO Name'][id_resultGO], dataFrame_GO['Type GO'][id_resultGO], dataFrame_GO['Function GO'][id_resultGO]]
                            infos_of_gene = [dataFrame_overlapping["Chr ID"][id_TE]] + [dataFrame_overlapping["TFBS ENCFF"][id_TE]] + [dataFrame_overlapping["TFBS ENCSR"][id_TE]] + [dataFrame_overlapping["Protein"][id_TE]]
                            infos_of_gene+= [dataFrame_overlapping["Start TE"][id_TE]] + [dataFrame_overlapping["End TE"][id_TE]] + [dataFrame_overlapping["Strand TE"][id_TE]] + [dataFrame_overlapping["Start TFBS"][id_TE]] + [dataFrame_overlapping["End TFBS"][id_TE]]
                            infos_of_gene+= list(dataFrame_Gene.iloc[id_gene])
                            infos_of_gene+= list_fromGO
                            print(infos_of_gene)
                            genic_environment.append(infos_of_gene)
                            
                    if atLeastOneGO == False:
                        list_fromGO = ["NA", "NA", "NA"]
                        list_fromGO = [dataFrame_GO['GO Name'][id_resultGO], dataFrame_GO['Type GO'][id_resultGO], dataFrame_GO['Function GO'][id_resultGO]]
                        infos_of_gene = [dataFrame_overlapping["Chr ID"][id_TE]] + [dataFrame_overlapping["TFBS ENCFF"][id_TE]] + [dataFrame_overlapping["TFBS ENCSR"][id_TE]] + [dataFrame_overlapping["Protein"][id_TE]]
                        infos_of_gene+= [dataFrame_overlapping["Start TE"][id_TE]] + [dataFrame_overlapping["End TE"][id_TE]] + [dataFrame_overlapping["Strand TE"][id_TE]] + [dataFrame_overlapping["Start TFBS"][id_TE]] + [dataFrame_overlapping["End TFBS"][id_TE]]
                        infos_of_gene+= list(dataFrame_Gene.iloc[id_gene])
                        infos_of_gene+= list_fromGO
                        print(infos_of_gene)
                        genic_environment.append(infos_of_gene)

        id_TE+=1
                        
    # convert to panda format
    list_genes_all = [x for x in genic_environment if x != []]
    dataFrame_genic_environment = pd.DataFrame(list_genes_all, columns=['Chr ID', 'TFBS ENCFF', 'TFBS ENCSR', 'Protein', 'Start TE', 'End TE', 'Strand TE', 'Start TFBS', 'End TFBS', 'Chr', 'Chr ID NCBI', 'Start Gene', 'End Gene', 'Sens Gens', 'Type', 'Gene ID', 'Gene Name', 'Function', 'GO Name', 'Type GO', 'Function GO'])

    return dataFrame_genic_environment