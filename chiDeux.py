#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import RetrieveGO
import pandas as pd
import os
from scipy.stats import chi2_contingency
import seaborn as sns; sns.set()
import numpy as np
import plotly.graph_objects as go



program_description = ''' ************PROGRAM DESCRIPTION************
    Statistic analysis to compare Gene Ontology proportions
    between TE having overlapped a TFBS at least 5 times (select only one TFBS) and TE not overlapped TFBS
    For TE not overlapping, we select only GO having same GO as TE having overlapped one TFBS
   '''
   
   
   

def create_parser(): # retrieve arguments before start the program
    
    parser = argparse.ArgumentParser(add_help=True, 
                                     description=program_description)
    
    # TEname is the name of the TE family we want to study
    parser.add_argument('-TE','--TEname', 
                        help="Name of the transposable elements family",
                        type=str,
                        default=None,
                        required=True) # necessary to indicate the TE name
    return parser
    


    
def parse_arguments(): # return a dictionary of arguments    
    
    parser = create_parser() # creation of the parser
    args = parser.parse_args() # parse the arguments supplied by the user
    dict_args = dict(args.__dict__)
    return dict_args




def checkFolder(path): # if a folder doesn't exist, we create it
    if not os.path.exists(path):
        os.mkdir(path)
        
        
        
        
def retrieveGO(table, proteinTFBS, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances, overlap):
    
    ''' Three parameters
    
        1. dataFrame_Gene: dataframe containing all genes from NCBI
            Its columns: ['Chr Name', 'Chr ID', 'Start', 'End', 'Sens', 'Type', 'ID', 'Gene Name', 'Function']
        
        2. dataFrame_GO: dataframe containing gene ontology
            Its columns: ['Gene ID', 'GO Name', 'Type GO', 'Function GO']
        
        3. dataFrame_TwoShorterDistances: dataFrame containing distances where we can found the closest genes of overlaps
            Its columns: ['TE Name', 'Overlap TE/TFBS', 'Chr', 'Start TE', 'End TE', 'Strand TE', 'Shorter Distance Left', 'Gene Name Left', 'Gene Strand Left', 'Gene Function Left', 'Shorter Distance Right', 'Gene Name Right', 'Gene Strand Right', 'Gene Function Right', 'Gene Name Center', 'Gene Strand Center', 'Gene Function Center'] '''

    
    # List of data structure
    gene_ontology = []

    id_TE = 0
    while id_TE < len(table): # Browse file in terms of TE occurences
        print(id_TE+1, '/', len(table))
        
        # Chromosome of TE occurences
        chr_TE = table["Chr"][id_TE]
        chr_TE = chr_TE.replace('chr', '')
        # Begin of TE
        start_TE = table["Start TE"][id_TE]
        start_TE = int(start_TE)
        # End of TE
        end_TE = table["End TE"][id_TE]
        end_TE = int(end_TE)
        
        # Browse file containing genes close to TE
        idOccurences=0
        while idOccurences < len(dataFrame_ShorterDistances):
            if dataFrame_ShorterDistances['Start TE'][idOccurences] == start_TE:
                gene_left = dataFrame_ShorterDistances['Gene Name Left'][idOccurences]
                gene_right = dataFrame_ShorterDistances['Gene Name Right'][idOccurences]
                gene_center = dataFrame_ShorterDistances['Gene Name Center'][idOccurences]
            idOccurences+=1       
        
        
        # Compare TE with coordinates of genes from NCBI
        len_dfGenes = len(dataFrame_Gene)
        id_gene = 0
        while id_gene < len_dfGenes:            
            # Chromosome of gene
            chr_gene = dataFrame_Gene["Chr Name"][id_gene]
            # Begin of gene
            start_gene = dataFrame_Gene["Start"][id_gene]
            start_gene = int(start_gene)
            # End of gene
            end_gene = dataFrame_Gene["End"][id_gene]
            end_gene = int(end_gene)
            # Gene Name
            gene_name = dataFrame_Gene["Gene Name"][id_gene]
                            
            if chr_gene == chr_TE:        

                if gene_name in [gene_left, gene_right, gene_center]:
                    geneID_fromGene = dataFrame_Gene["ID"][id_gene].split(',')[0]
                                        
                    # TE informations
                    infos = [overlap, proteinTFBS]
                    infos+= list(table.loc[id_TE][['Chr', 'Strand TE', 'Start TE', 'End TE']])
                    # Gene informations
                    infos+= list(dataFrame_Gene.loc[id_gene][['Sens', 'Start', 'End', 'ID', 'Gene Name', 'Type', 'Function']])
                    
                    # Retrieve all Gene Ontology of this current gene
                    masque = dataFrame_GO['Gene ID'] == geneID_fromGene
                    resultsGO = dataFrame_GO[masque]
                    
                    for id_resultGO in list(resultsGO.index):
                        # Assign value to data structure
                        # 2 columns containing names of TFBS associated (ENCFF and ENCSR) + informations about genes (retrieve all) + informations about GO
                        # merge all into the same list
                        
                        list_fromGO = list(dataFrame_GO.loc[id_resultGO][['GO Name', 'Type GO', 'Function GO']])
                        infos+= list_fromGO
                        gene_ontology.append(infos)
                        
                        # Reset infos list
                        infos = [overlap, proteinTFBS]
                        infos+= list(table.loc[id_TE][['Chr', 'Strand TE', 'Start TE', 'End TE']])
                        infos+= list(dataFrame_Gene.loc[id_gene][['Sens', 'Start', 'End', 'ID', 'Gene Name', 'Type', 'Function']])
                    
            id_gene+=1
        id_TE+=1
                        
    # convert to panda format
    list_GO_all = [x for x in gene_ontology if x != []]
    dataFrame_GO_environment = pd.DataFrame(list_GO_all, columns=['Overlap', 'Protein TFBS', 'Chr', 'Strand TE', 'Start TE', 'End TE', 'Strand Gene', 'Start Gene', 'End Gene', 'Gene ID', 'Gene Name', 'Gene Type', 'Gene Function', 'GO Name', 'Type GO', 'Function GO'])

    return dataFrame_GO_environment
        



def fetchGO_OverlappingHavingMoreFiveTFBS(TEname, dataFrame_Overlapping, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances):
    
    '''
        For each TFBS, fetch Gene Ontology of occurences TE having overlapped only one TFBS at east 5 times
    '''
    
    proteins = dataFrame_Overlapping.Protein.unique() # retrieve all TFBS of overlaps
    
    for protein in proteins:
        
        masque = dataFrame_Overlapping['Protein'] == protein
        df_Overlapping_aTFBS = dataFrame_Overlapping[masque].reset_index()
        if len(df_Overlapping_aTFBS) > 4:
            # Create folder
            checkFolder('../Results/ChiDeux/{}/{}'.format(TEname, protein))
            # Create file for each protein         
            filename = '../Results/ChiDeux/{}/{}/GO_TEoverlapping{}.tsv'.format(TEname, protein, protein)
            if not os.path.exists(filename):
                table_GO_environment = retrieveGO(df_Overlapping_aTFBS, protein, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances, 'Yes')
                table_GO_environment.to_csv(filename, sep="\t", index=False)
    



def fetchGO_TENotOverlapping(TEname, dataFrame_NotOverlapping, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances):
      
    '''
        Fetch all Gene Ontology of occurences TE not having overlapped a TFBS
    '''
    
    filename = '../Results/ChiDeux/{}/GO_TEnotOverlapping_All.tsv'.format(TEname)
    if not os.path.exists(filename):
        table_GO_environment = retrieveGO(dataFrame_NotOverlapping, None, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances, 'No')
        table_GO_environment.to_csv(filename, sep="\t", index=False)
        



def keepGOSameAsOverlapping(TEname, directory_GO_TEsoverlappingTFBS, df_removeGO, title):
      
    '''
        Keep only Gene Ontology same as those having overlapped one TFBS
    '''
    
    for root, dirs, files in os.walk(directory_GO_TEsoverlappingTFBS): # Retrieve names of proteins TFBS presented at least 5 times
        for protein in dirs: # fetch names of directories
        
            if protein != 'GO_TEnotOverlapping_All':
                
                # Retrieve GO of TE having overlapping a TFBS
                df_GOoverlappingTFBS = pd.read_csv('../Results/ChiDeux/{}/{}/GO_TEoverlapping{}.tsv'.format(TEname, protein, protein), sep="\t")
                    
                if title == 'TEoverlapping VS TEnotOverlapping':
                    filename_output = '../Results/ChiDeux/{}/{}/GO_TEnotOverlapping_sameAs{}.tsv'.format(TEname, protein, protein)
                    # Remove GO not same as GO of TE having overlapping a TFBS
                    df_GO_SameAsOverlapTFBS = df_removeGO.loc[df_removeGO['Function GO'].isin(df_GOoverlappingTFBS['Function GO']), :]
                    # Save as CSV
                    df_GO_SameAsOverlapTFBS.to_csv(filename_output, sep="\t", index=False)
                    # Do statistic test and save as txt file
                    chiDeux(TEname, protein, df_GOoverlappingTFBS, df_GO_SameAsOverlapTFBS, title)
    
                    
                elif title == 'TEoverlapping VS AllGenes':
                    filename_output = '../Results/ChiDeux/{}/{}/GO_AllGenes_sameAs{}.tsv'.format(TEname, protein, protein)
                    # Remove GO not same as GO of TE having overlapping a TFBS
                    df_GO_SameAsOverlapTFBS = df_removeGO.loc[df_removeGO['Function GO'].isin(df_GOoverlappingTFBS['Function GO']), :]
                    # Save as CSV
                    df_GO_SameAsOverlapTFBS.to_csv(filename_output, sep="\t", index=False)
                    # Do statistic test and save as txt file
                    chiDeux(TEname, protein, df_GOoverlappingTFBS, df_GO_SameAsOverlapTFBS, title)
    
    
                elif title == 'AllTE VS AllGenes':
                    filename_output = '../Results/ChiDeux/{}/{}/GO_AllGenes_sameAs{}andAllTE.tsv'.format(TEname, protein, protein)
                    filename_output_allTE = '../Results/ChiDeux/{}/{}/GO_AllTE_sameAs{}.tsv'.format(TEname, protein, protein)

                    # GO of all TE not overlapping a TFBS
                    df_GOallTENotoverlappingTFBS = pd.read_csv('../Results/ChiDeux/{}/{}/GO_TEnotOverlapping_sameAs{}.tsv'.format(TEname, protein, protein), sep="\t")
                    
                    # Concat for retrieve GO of all TE regarding of one TFBS
                    df_GOallTE = pd.concat([df_GOoverlappingTFBS, df_GOallTENotoverlappingTFBS])
                    df_GOallTE.to_csv(filename_output_allTE, sep="\t", index=False)
                    
                    # Remove GO not same as GO of TE having overlapping a TFBS
                    df_GO_SameAsOverlapTFBS = df_removeGO.loc[df_removeGO['Function GO'].isin(df_GOallTE['Function GO']), :]
                    # Save as CSV
                    df_GO_SameAsOverlapTFBS.to_csv(filename_output, sep="\t", index=False)
                    chiDeux(TEname, protein, df_GOallTE, df_GO_SameAsOverlapTFBS, title)
                


def chiDeux(TEname, protein, df_GO_Overlapping, df_GO_SameAsOverlapTFBS, title):
    
    '''
        1. Chi-Deux between TE overlapping a defined TFBS and TE not overlapping a TFBS for which we keep only GO same as GO of TE overlapping a defined TFBS
        2. Chi-Deux: TE overlapping VS all Genes
        3. Chi-Deux: all TE VS all Genes
    '''
    
    # List of data structure
    pvalues = []
    
    # add collumn if its data frame of all genes
    if title == 'TEoverlapping VS AllGenes' or title == 'AllTE VS AllGenes':
        df_GO_SameAsOverlapTFBS['Overlap'] = 'All Genes'
        
    if title == 'AllTE VS AllGenes':
        df_GO_Overlapping['Overlap'] = 'Both'
    
    filenameOutput = '../Results/ChiDeux/{}/{}/chiDeux-{}.txt'.format(TEname, protein, title.replace(' ', ''))
    output = open(filenameOutput, 'w')
    
    df = pd.concat([df_GO_Overlapping, df_GO_SameAsOverlapTFBS]) # concatenation without join
    output.write("TE FAMILY: {}\n".format(TEname))
    if title == 'TEoverlapping VS TEnotOverlapping':
        output.write("TABLE WITH TE OVERLAPPING {} TFBS, AND TE NOT OVERLAPPING ONLY SAME GENE ONTOLOGY AS TE OVERLAPPING:\n".format(protein))
        Ho = "Ho: Homogeneity between Gene Ontology of TE overlapping and those of TE not overlapping, and which we kept only same GO as TE having at least overlapped five times one TFBS."
    
    elif title == 'TEoverlapping VS AllGenes':
        output.write("TABLE WITH TE OVERLAPPING {} TFBS, AND ALL GENES ONLY SAME GENE ONTOLOGY AS TE OVERLAPPING:\n".format(protein))
        Ho = "Ho: Homogeneity between Gene Ontology of TE overlapping and those of all genes, and which we kept only same GO as TE having at least overlapped five times one TFBS."
    
    elif title == 'AllTE VS AllGenes':
        output.write("TABLE WITH TE OVERLAPPING {} TFBS + TE NOT OVERLAPPING THIS TFBS, AND ALL GENES ONLY SAME GENE ONTOLOGY AS GO OF PREVIOUS TE:\n".format(protein))
        Ho = "Ho: Homogeneity between Gene Ontology of all TE, and those of all genes and which we kept only same GO as TE having at least overlapped five times one TFBS."
    output.write(str(df))
    
    X = 'Function GO'
    Y = "Overlap"
    tableContingency = pd.pivot_table(df[['Function GO', 'Overlap']], index=X, columns=Y, aggfunc=len).fillna(0).copy().astype(int)

    output.write("\n\nTOTAL OF ROWS:\n")
    output.write(str(tableContingency.sum(axis = 1)))

    output.write("\n\nTOTAL OF COLUMNS:\n")
    output.write(str(tableContingency.sum(axis = 0)))
    
    output.write("\n\nTABLE OF CONTINGENCY:\n")
    output.write(str(tableContingency))
    
    output.write("\n\nTEST:\n")
    output.write("\n"+Ho)
    chi2, pvalue, degrees, expected = chi2_contingency(tableContingency)
    output.write('\nChi2: '+ str(chi2))
    output.write('\nP-value: '+ str(pvalue))
    
    if pvalue > 0.05:
        ccl_test = 'Ho Accepted'
    else:
        ccl_test = 'Ho Rejected'

    
    if title == 'TEoverlapping VS TEnotOverlapping':
        pvalues.append(['TE overlapping VS TE not overlapping', protein, pvalue, ccl_test])
    elif title == 'TEoverlapping VS AllGenes':
        pvalues.append(['TE overlapping VS all Genes', protein, pvalue, ccl_test])
    elif title == 'AllTE VS AllGenes':
        pvalues.append(['All TE VS all Genes', protein, pvalue, ccl_test])
    
    
    output.write('\nDegrees: '+ str(degrees))
    output.write('\nExpected Values: '+ str(expected))
    
    output.close()
    
    # convert to panda format
    list_pvalues = [x for x in pvalues if x != []]
    dataframe_pvalues = pd.DataFrame(list_pvalues, columns=['Type ChiDeux', 'TFBS', 'Pvalue', 'Conclusion Test'])

    output_file_pvalues = '../Results/ChiDeux/{}/pvalues.tsv'.format(TEname)
    
    if not os.path.exists(output_file_pvalues):
        dataframe_pvalues.to_csv(output_file_pvalues, sep='\t', mode='w', header=True, index=False)
    else:
        dataframe_pvalues.to_csv(output_file_pvalues, sep='\t', mode='a', header=False, index=False)
            


def discrete_colorscale(bvals, colors):
    """
    bvals - list of values bounding intervals/ranges of interest
    colors - list of rgb or hex colorcodes for values in [bvals[k], bvals[k+1]],0<=k < len(bvals)-1
    returns the plotly  discrete colorscale
    """
    if len(bvals) != len(colors)+1:
        raise ValueError('len(boundary values) should be equal to  len(colors)+1')
    bvals = sorted(bvals)     
    nvals = [(v-bvals[0])/(bvals[-1]-bvals[0]) for v in bvals]  #normalized values
    
    dcolorscale = [] #discrete colorscale
    for k in range(len(colors)):
        dcolorscale.extend([[nvals[k], colors[k]], [nvals[k+1], colors[k]]])
    return dcolorscale


    
def heatmap(dataFrame, TEname):

    # https://chart-studio.plotly.com/~empet/15229.embed
    
    l1 = dataFrame.loc[dataFrame["Type ChiDeux"] == "TE overlapping VS TE not overlapping", "Pvalue"]
    l2 = dataFrame.loc[dataFrame["Type ChiDeux"] == "TE overlapping VS all Genes", "Pvalue"]
    l3 = dataFrame.loc[dataFrame["Type ChiDeux"] == "All TE VS all Genes", "Pvalue"]
    
    data = [list(l1), list(l2), list(l3)]
    
    bvals = [0, 5e-2, 5.1e-2]

    colors = ['#DE4747', '#1873BC'] # red < or = 0.05 and blue > 0.05

    dcolorsc = discrete_colorscale(bvals, colors)
    bvals = np.array(bvals)
    tickvals = [np.mean(bvals[k:k+2]) for k in range(len(bvals)-1)] #position with respect to bvals where ticktext is displayed
    ticktext = [f'<{bvals[1]}'] + [f'{bvals[k]}-{bvals[k+1]}' for k in range(1, len(bvals)-2)]+[f'>{bvals[-2]}']
    print(tickvals)
    print(ticktext)
    heatmap = go.Heatmap(z=data,
                         x=dataFrame["TFBS"].unique(),
                         y=dataFrame["Type ChiDeux"].unique(),
                         colorscale = dcolorsc, 
                         colorbar = dict(thickness=10, 
                                     tickvals=tickvals, 
                                     ticktext=ticktext)
                         )
    
    fig = go.Figure(data=[heatmap])
    fig.update_layout(title = "ChiDeux of homogeneity to compare proportions of gene ontology",)
    #fig.update_xaxes(side="top")
    fig.update_layout(width=1000, height=300)
    fig.write_html("../Results/ChiDeux/{}/heatmap{}.html".format(TEname, TEname))
 
    

def main(TEname):

    #####################
    #### INPUT files ####
    #####################
    
    pathOverlapping = '../Results/Overlapping/overlapping_{}-withoutDuplicateTFBS.tsv'.format(TEname)
    path_genes = '../Data/genesNCBI_Human_Annotation.tsv'
    path_GO = '../Data/DATA_GeneOntology.txt'
    filename_input = '../Results/ShorterDistances-GenesTEs/{}.tsv'.format(TEname)

    ############################
    #### Import informations ###
    #### Dataframes format  ####
    ############################
    
    dataFrame_Gene = pd.read_csv(path_genes, sep="\t")
    dataFrame_GO = RetrieveGO.parseGO(path_GO)
    
    dataFrame_ShorterDistances = pd.read_csv(filename_input, sep="\t")
    dataFrame_Overlapping = pd.read_csv(pathOverlapping, sep="\t")
    dataFrame_NotOverlapping = dataFrame_ShorterDistances.loc[dataFrame_ShorterDistances['Overlap TE/TFBS'] == 'No', :].reset_index()
    
    ################
    #### OUTPUT ####
    ################
    
    checkFolder('../Results/ChiDeux/')
    checkFolder('../Results/ChiDeux/{}/'.format(TEname))
    
    # Fetch GO of TE overlapping at least 5 times a TFBS
    fetchGO_OverlappingHavingMoreFiveTFBS(TEname, dataFrame_Overlapping, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances)
    
    # Fetch GO of TE occurences not having overlapped a TFBS
    fetchGO_TENotOverlapping(TEname, dataFrame_NotOverlapping, dataFrame_Gene, dataFrame_GO, dataFrame_ShorterDistances)

    ## Keep only same GO as overlapping, for TE NOT OVERLAPPING A TFBS
    directory_GO_TEsoverlappingTFBS = '../Results/ChiDeux/{}/'.format(TEname)
    # Filename of data frame where we will remove GO not same as GO of TE overlapping one TFBS at east 5 times
    filename_GOtoRemove_TENotOverlap = '../Results/ChiDeux/{}/GO_TEnotOverlapping_All.tsv'.format(TEname)
    # Input we want to transform
    df_GO_allNotOverlapping = pd.read_csv(filename_GOtoRemove_TENotOverlap, sep="\t")
    keepGOSameAsOverlapping(TEname, directory_GO_TEsoverlappingTFBS, df_GO_allNotOverlapping, "TEoverlapping VS TEnotOverlapping")

    ## Keep only same GO as overlapping, for gene ontology of ALL GENES
    keepGOSameAsOverlapping(TEname, directory_GO_TEsoverlappingTFBS, dataFrame_GO, "TEoverlapping VS AllGenes")
    
    ## Keep only same GO as ALL TE, for gene ontology of ALL GENES
    keepGOSameAsOverlapping(TEname, directory_GO_TEsoverlappingTFBS, dataFrame_GO, "AllTE VS AllGenes")

    # p-values output
    data = pd.read_csv('../Results/ChiDeux/{}/pvalues.tsv'.format(TEname), sep="\t")
    # plot heatmap
    heatmap(data, TEname)
    
    
    
    
if __name__ == '__main__':
    arguments = parse_arguments()
    main(**arguments)