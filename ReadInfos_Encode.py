#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import os



########################################################################################################################
### Retrieve all files named as "ENCFFXXXXXX.bed"
########################################################################################################################

def retrieveTFBSNames(directory):
    
    list_TFBSnames = []
    
    for filename in os.listdir(directory):
        l = len(filename)
        if(filename[0:5] == "ENCFF" and filename[l-4:l] == ".bed" and l==15):
            list_TFBSnames.append(filename.replace(".bed",""))
            
    return list_TFBSnames


########################################################################################################################
### Extract experiment informations about ChIP-seq
########################################################################################################################

def parseChipseqFile(file_experiments, list_of_TFBS):
    
    list_of_TFBS_update = [] # all ENCFF name
    list_ENCFF = [] # informations about each ENCFF name
        
    #file = open(file_experiments, "r")
    file = file_experiments
    file.readline() # remove head
    file.readline()
    
    for line in file.readlines():
        
        decoupe = line.split('\t')
        
        nameENCSR = decoupe[1]
        
        namesENCFF = decoupe[12].replace('/files/','').replace('/','').split(',')
        
        for name_ENCFF in namesENCFF:
            if name_ENCFF in list_of_TFBS: # conserve only our TFBS
                list_of_TFBS_update.append(name_ENCFF)
                list_ENCFF.append([nameENCSR, decoupe[4], decoupe[5], decoupe[6], decoupe[7], decoupe[8], decoupe[9], decoupe[13], decoupe[17], decoupe[18], decoupe[19]])


    list_chipseq = [x for x in list_ENCFF if x != []]
    dataFrame_ExperimentsChipseq = pd.DataFrame(list_chipseq, index= list_of_TFBS_update,
                                             columns=["ENCSR", "Target of assay", "Target gene symbol", "Biosample summary", "Biosample term name", "Description", "Lab", "Related series", "Linked antibody", "Organism", "Life stage"] )

    
    file.close
    return dataFrame_ExperimentsChipseq, list_of_TFBS_update



########################################################################################################################
### Read ENCODE file (ChIP-Seq) : extract data of one TFBS (Transcription Factor Binding Site)
########################################################################################################################

def retrieve_TFBS(directory, dataFrame_ExperimentsChipseq, one_TFBS_name):

    Infos_TFBS = []
    # first read the file lines
    fileEncode = directory + one_TFBS_name + ".bed"
    file = open(str(fileEncode), "r")
    lines = file.readlines()
    
    #file.readline()
    
    if lines != []:
        #for i in range(0, len(lines), 1):
        for line in lines:
            # remove the unnecessary blank spaces
            line = line.rstrip()
            line = ' '.join(line.split())
            decoupe = line.split(' ')
    		
            # for each line get the different value
            chromosome = decoupe[0]
            start = decoupe[1]
            end = decoupe[2]
            size = int(end) - int(start)
            
            # ENCSR
            TFBS_ENCSR = dataFrame_ExperimentsChipseq['ENCSR'][one_TFBS_name]
            
            # protein used in this ChIP-seq
            protein = dataFrame_ExperimentsChipseq['Target of assay'][one_TFBS_name]
            
            # biosample used in this ChIP-seq
            biosample = dataFrame_ExperimentsChipseq['Biosample summary'][one_TFBS_name]
        
            # assign value to data structure
            list_a_TFBS = [one_TFBS_name, TFBS_ENCSR, chromosome, start, end, size, protein, biosample]
        
            Infos_TFBS.append(list_a_TFBS)  
        
    #convert to panda format
    listTFBS = [x for x in Infos_TFBS if x != []]
    dataFrame_TFBS = pd.DataFrame(listTFBS, columns=['TFBS_ENCFF', 'TFBS_ENCSR', 'Chr', 'Start', 'End', 'Size', 'Protein', 'Biosample'])
        
    return dataFrame_TFBS