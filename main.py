#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import argparse
from pathlib import Path

import ReadInfos_TE
import ReadInfos_RepeatMasker
import ReadInfos_NCBIAnnotations
import ReadInfos_Encode
import Overlapping
import RetrieveGenes
import RetrieveGO


program_description = ''' ************PROGRAM DESCRIPTION************
    Overlap a Transposable Elements family with Transcription Factor Binding Sites (TFBS).
    Then, retrieve genic environment around overlaps.
   '''



def create_parser(): # retrieve arguments before start the program
    
    parser = argparse.ArgumentParser(add_help=True, 
                                     description=program_description)
    
    ########################################################################################################################
    ### INPUT
    ########################################################################################################################
      
    # TEname is the name of the TE we want to study
    parser.add_argument('-TE','--TEname', 
                        help="Name of the transposable elements family",
                        type=str,
                        default=None,
                        required=True) # necessary to indicate the TE name
    
    
    # To extract RepeatMasker data (Transposable Elements)
    parser.add_argument('-RM', '--RepeatMaskerFile',
                        help="Path to the RepeatMasker file",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
    
    
    # To extract ENCODE data (ChIP-Seq)
    parser.add_argument('-encode', '--EncodeDirectory',
                        help="Directory of ChIP-Seq file (of Transcription Factor Binding Sites)",
                        default=None,
                        type=str,
                        required=True)
    
    
    # File containing all consensus sequences
    parser.add_argument('-repbase', '--RepbaseFile',
                        help="Path to the Repbase file containing all consensus sequences of Transposable Elements.",
                        default=None,
                        type=str,
                        required=True)
    
    
    # File containing informations about chIPseq experiments
    parser.add_argument('-chipseq', '--chipseqExperimentsFile',
                        help="Path to the ChIP-Seq experiments file, containing informations about each TFBS.",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
    
    
    # To extract NCBI annotation of genes
    parser.add_argument('-genomeFile', '--fileGFFgenome',
                        help="Path to the GFF genome file from NCBI",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
    
    
    # File containing Gene Ontology informations
    parser.add_argument('-GOfile', '--geneOntologyFile',
                        help="Path to the Gene Ontology file.",
                        default=None,
                        type=str,
                        required=True)
    
    
    # percent of ChIP-Seq peak for which must at least match with the TE
    parser.add_argument('-peak', '--nts_NecessaryInPeak',
                        help="Choose number of nucleotides neccessary in peak, furthermore middle of peak must be in TE",
                        default=10,
                        type=float,
                        required=False)
    
    return parser




def parse_arguments(): # return a dictionary of arguments
    # Example: {"TEname": "L3", "RepeatMasker": "'../data/RepeatMasker_Human_Result/RepeatMasker_Human.fna.out'"}
    
    parser = create_parser() # creation of the parser
    args = parser.parse_args() # parse the arguments supplied by the user
    dict_args = dict(args.__dict__)
    
    return dict_args



def createFolder(nameFolder):
    if not os.path.exists(nameFolder):
        os.mkdir(nameFolder)  



def main(TEname, RepeatMaskerFile, EncodeDirectory, fileGFFgenome, RepbaseFile, chipseqExperimentsFile, geneOntologyFile, nts_NecessaryInPeak):
    
    ########################################################################################################################
    ### Extract data
    ########################################################################################################################
    
    # Create a folder for results
    createFolder("../Data")
    createFolder("../Data/ChIPseq")
    createFolder("../Results")
    createFolder("../Results/TE")
    createFolder("../Results/Overlapping")
    createFolder("../Results/Genic-Environment")
    createFolder("../Results/ShorterDistances-GenesTEs/")
    createFolder("../Results/ShorterDistances-GenesTEs/Plot/")
    
    
    
    
    ##########
    ### TE ###
    ##########
    
    # Extract the Repbase data
    # Repbase is a dictionary between the name of the TE and the group to which it belongs
    Repbase = ReadInfos_TE.LireRepbase(RepbaseFile)
    
    # Extract the TE group the user sequence belongs
    # mySuperfamilyTE donne le nom du groupe TE
    mySuperfamilyTE, SequenceConsensus = ReadInfos_TE.GetSuperfamily(RepbaseFile, TEname)
    
    # RepeatMaskerFile is the RepeatMasker file
    TE_file = os.path.basename(str(RepeatMaskerFile)) # Get only the last part of the path without extension file
    index_of_dot = TE_file.index('.')
    TE_file = TE_file[:index_of_dot] # without extension
    
    # Name of output file
    path_MyTE = "../Results/TE/{}_{}.tsv".format(TEname, TE_file)
    
    # Create dataframe containing informations about TE
    if not os.path.exists(path_MyTE):
        dataFrame_MyTE, dataFrame_OtherTE = ReadInfos_RepeatMasker.ReadRepeatMasker(RepeatMaskerFile, TEname, Repbase)
        # Save on tsv file
        dataFrame_MyTE.to_csv(path_MyTE, sep='\t', index=False)
    else:
        # Read tsv file already created
        dataFrame_MyTE = pd.read_csv(path_MyTE, sep="\t")
    
    
    
    
    ############
    ### TFBS ###
    ############
    
    # Extract names of TFBS
    list_of_TFBS = ReadInfos_Encode.retrieveTFBSNames(EncodeDirectory)
    
    # Get only the last part of the path
    folder_name_TFBS = Path(EncodeDirectory).stem
    createFolder("../Data/TFBS-{}".format(folder_name_TFBS))

    # Extract chIP-Seq informations
    path_ChipSeq = "../Data/ChIPseq/experimentsChipSeq-GRCh38.tsv"
    dataFrame_ExperimentsChipseq,list_of_TFBS_update = ReadInfos_Encode.parseChipseqFile(chipseqExperimentsFile, list_of_TFBS)
    if not os.path.exists(path_ChipSeq):
        dataFrame_ExperimentsChipseq.to_csv(path_ChipSeq, sep="\t") # conserve in file (tsv format)
    
    
    #############
    ### genes ###
    #############
    
    # Get only the last part of the path without extension file
    GFF_file = Path(str(fileGFFgenome)).stem
    
    # Name of ouput file
    path_genesNCBI = "../Data/genesNCBI_{}.tsv".format(GFF_file)
    
    if not os.path.exists(path_genesNCBI):
        # Extract data from NCBI file (format GFF): informations like genes, ncRNA..
        nbSeq_Assemble, nameOrganism, maxSize, dataFrame_Organism, dataFrame_Gene, dataFrame_GeneExon, dataFrame_PseudoGene, dataFrame_PseudoGeneExon, dataFrame_ncRNA, dataFrame_Regulator = ReadInfos_NCBIAnnotations.ReadGFF(fileGFFgenome)
        # Save on tsv file
        dataFrame_Gene.to_csv(path_genesNCBI, sep='\t', index=False)
    else:
        # Read tsv file already created
        dataFrame_Gene = pd.read_csv(path_genesNCBI, sep="\t")
    
    
      
        
    ########################################################################################################################
    ### Main
    ########################################################################################################################
    
    ###############
    ### Overlap ###
    ###############
    
    print('\n\n ********** OVERLAPPING TE/TFBS ********** ')

    # Ouput file
    path_overlapForATEFamily = "../Results/Overlapping/overlapping_{}.tsv".format(TEname)
    
    for TFBS_name in list_of_TFBS_update:
        print("TFBS:", TFBS_name, " ", list_of_TFBS_update.index(TFBS_name)+1, "/", len(list_of_TFBS_update))
        # Name of ouput file
        path_TFBS = "../Data/TFBS-{}/{}.tsv".format(folder_name_TFBS, TFBS_name)
            
        if not os.path.exists(path_TFBS):
            dataFrame_TFBS = ReadInfos_Encode.retrieve_TFBS(EncodeDirectory, dataFrame_ExperimentsChipseq, TFBS_name)
            dataFrame_TFBS.to_csv(path_TFBS, sep="\t", index=False)
            
        # Run overlapping script
        dataFrame_overlapping = Overlapping.overlapping(TEname, dataFrame_MyTE, dataFrame_TFBS, nts_NecessaryInPeak)
        # Save on csv file, adding informations if file is already present
        if not os.path.exists(path_overlapForATEFamily):
            dataFrame_overlapping.to_csv(path_overlapForATEFamily, sep='\t', mode='w', header=True, index=False)
        else:
            dataFrame_overlapping.to_csv(path_overlapForATEFamily, sep='\t', mode='a', header=False, index=False)
        
    # Retrieve data frame overlapping directly in the file    
    dataFrame_overlapping = pd.read_csv(path_overlapForATEFamily, sep="\t")
    # Remove occurences of TE with same chrosomosome, start end and same TFBS
    dataFrame_overlapping_withoutDuplicateTFBS = dataFrame_overlapping.drop_duplicates(subset=['Chr', 'Start TE', 'End TE', 'Start TFBS', 'End TFBS', 'Protein'], keep='first').reset_index()
    dataFrame_overlapping_withoutDuplicateTFBS.to_csv("../Results/Overlapping/overlapping_{}-withoutDuplicateTFBS.tsv".format(TEname), sep="\t", index=False)

            
    
    
    
    #################################################################
    ### Look at the genic environment and integrate gene ontology ###
    #################################################################

    #############
    ## Outputs ##
    #############
    # File Path for TSV output (framadate)
    path_Dist_GenesTE = '../Results/ShorterDistances-GenesTEs/{}.tsv'.format(TEname)
    # File Path for HTML output (graphic)
    path_Graph = '../Results/ShorterDistances-GenesTEs/Plot/{}.html'.format(TEname)
    # Path for genic environment output
    pathGOEnvironment= '../Results/Genic-Environment/genic-environment_{}.tsv'.format(TEname)


    ###################
    ## Retrieve Genes #
    ###################
    print('\n\n ********** RETRIEVE GENES ********** ')
    dataFrame_distances = RetrieveGenes.calculateDistances_Genes_TEs(TEname, dataFrame_Gene, dataFrame_MyTE, dataFrame_overlapping_withoutDuplicateTFBS)
    dataFrame_distances.to_csv(path_Dist_GenesTE, sep='\t', header=True, index=False)
    # Plot histogram of distances between TE and Genes around
    RetrieveGenes.histogramDistances_GenesTEs(dataFrame_distances, 'Shorter Distance', 'Histogram: Shorter distances between each occurence of {} TE family and gene the closest'.format(TEname), path_Graph)

    ############################
    ## Retrieve Gene Ontology ##
    ############################
    print('\n\n ********** RETRIEVE GENE ONTOLOGY ********** ')
    dataFrame_GO = RetrieveGO.parseGO(geneOntologyFile)
    dataFrame_GOEnvironment = RetrieveGO.retrieveGO(dataFrame_overlapping_withoutDuplicateTFBS, dataFrame_Gene, dataFrame_GO, dataFrame_distances)
    dataFrame_GOEnvironment.to_csv(pathGOEnvironment, sep="\t", index=False)




if __name__ == '__main__':
    arguments = parse_arguments()
    main(**arguments)