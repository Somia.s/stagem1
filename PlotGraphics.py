#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd




program_description = ''' ************PROGRAM DESCRIPTION************
    Overlap a Transposable Elements family with Transcription Factor Binding Sites (TFBS).
    Then, retrieve genic environment around overlaps.
    Results in the form of graphics.
   '''



def create_parser(): # retrieve arguments before start the program
    
    parser = argparse.ArgumentParser(add_help=True, 
                                     description=program_description)
    
    ########################################################################################################################
    ### INPUT
    ########################################################################################################################
      
    # To extract of one TE informations
    parser.add_argument('-TE', '--TE_file',
                        help="Path to the transposable element file",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
    
    
    # To extract TFBS informations
    parser.add_argument('-TFBS', '--TFBS_file',
                        help="Path to the transcription factor binding sites file",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
        
    
    # To extract overlapping informations
    parser.add_argument('-overlap', '--overlapping_file',
                        help="Path to the overlapping file",
                        default=None,
                        type=argparse.FileType('r'),
                        required=True)
    
    return parser




def parse_arguments(): # return a dictionary of arguments
    
    parser = create_parser() # creation of the parser
    args = parser.parse_args() # parse the arguments supplied by the user
    dict_args = dict(args.__dict__)
    
    return dict_args




def list_TFBS(file_chipseq):
    
    list_of_TFBS = []
    file_chipseq.readline()
    texte = file_chipseq.readlines()
    
    for line in texte:
        line = line.split('\t')
        list_of_TFBS.append(line[2])
    
    file_chipseq.close()
    
    return list_of_TFBS




def barPlot_TEs(tab, tab_TEnotOverlap, tabGenesGO, xlab_start, xlab_end, ylab, title, pathOutput):
    
    '''
        Plot transposable elements (TEs) on chromosomes in terms of genomic coordinates (bp)
        Parameters:
        tab: table containing all informations
        xlab_start: start of TE
        xlab_end: end of TE
        ylab: chromosome names
    '''
    
    fig = go.Figure()
    
    # add all chromosomes
    fig.add_trace(go.Bar(x=tab['Length Chr'], y=tab[ylab],
                    base=0, # start
                    orientation='h',
                    name='Chromosome',
                    text=tab['Chr ID'],
                    textposition='inside'
                ))
    
    # Add TE not having overlapped
    fig.add_trace(go.Bar(x=tab_TEnotOverlap['End TE']-tab_TEnotOverlap['Start TE']+500000, y=tab_TEnotOverlap[ylab],
                base=tab_TEnotOverlap['Start TE'], # start
                orientation='h',
                name='TE not overlapping',
                text=tab['Protein'],
                textposition='inside'
                ))
    
    # Add TE overlapping
    fig.add_trace(go.Bar(x=tab[xlab_end]-tab[xlab_start]+500000, y=tab[ylab],
                    base= tab[xlab_start], # start
                    orientation='h',
                    name='TE overlapping'))
    
    # Add Genes
    fig.add_trace(go.Bar(x=tabGenesGO['End Gene']-tabGenesGO['Start Gene']+500000, y=tabGenesGO[ylab],
                    base= tabGenesGO['Start Gene'], # start
                    orientation='h',
                    #text=[str('Gene ID: '+tabGenesGO['Gene ID']+'; GO Name:'+tabGenesGO['GO Name']+'; Type GO:'+tabGenesGO['Type GO']+'; Function GO:'+ tabGenesGO['Function GO'])],
                    text=tabGenesGO['Function GO'],
                    name='Gene'))
    
    # layout
    fig.update_layout(showlegend=True,
        title=title,
        xaxis_title="Genomic coordinates (bp)",
        yaxis_title="Chromosomes",
        font=dict(
            family="Courier New, monospace",
            size=12
            )
    )
    
    # Save as html ouput
    fig.write_html(pathOutput)


    
    
    
def barChartFrequencies(list_of_TFBS, TEname, tab_of_data, element, title_of_graph, path_output):
    '''
        Bar chart represents proportion of a element in a file containing set of occurences
    '''  
        
    df = pd.DataFrame() #initialize a empty framadate to contain occurences and their effectifs and frequencies
    df[element] = tab_of_data[element].unique()
    
    title_of_graph = title_of_graph.format(TEname, len(tab_of_data), len(df[element]))
    
    nbOccurences_columnName = 'Number of {} occurences'.format(element)
    df[nbOccurences_columnName] = 0 # column with frequencies of each TFBS, added to dataframe
    df['Frequency'] = 0 # column with frequencies of each TFBS, added to dataframe

    for gene in tab_of_data[element]: # element found during overlapping
        df.loc[df[element] == gene, nbOccurences_columnName]+=1 
    
    # Calculate frequencies for each TFBS proteins
    for gene in df[element]:
        df.loc[df[element] == gene,'Frequency'] = (df.loc[df[element] == gene, nbOccurences_columnName]/len(tab_of_data))
    
    # not count frequencies < 1%
    df.loc[df['Frequency'] < 0.01, element] = '< 0.01%' # Represent only % > 0.01s
    # Fetch total of TFBS having a frequency < 0.01%
    id_df=0
    sumElementsWeakFrequency = 0
    while id_df < len(df):
        if df[element][id_df] == '< 0.01%':
            sumElementsWeakFrequency+=df[nbOccurences_columnName][id_df]
        id_df+=1
    
    idDF = 0
    while idDF < len(df):
        if df[element][idDF] == '< 0.01%':
            df[nbOccurences_columnName][idDF] = sumElementsWeakFrequency
        idDF+=1
        
    print(df)
    
    
    # plot bar chart
    fig = px.bar(df, x=element, y='Frequency', 
                     text=nbOccurences_columnName, color=nbOccurences_columnName)
    fig.update_traces(textposition='outside')

    # layout
    fig.update_layout(
        title=title_of_graph.format(TEname, len(tab_of_data), len(tab_of_data[element])),
        font=dict(
            family="Courier New, monospace",
            size=11
            )
    )
    
    # Save as html ouput
    fig.write_html(path_output) 
    
        
    

def includeAbsentChr(table, tab_chrom):
    '''
        Show all chromosomes, even if there is not TE
    '''
    for num_chr in list(tab_chrom.Chr.unique()):
        if num_chr not in list(table.Chr.unique()):
            chrID = list(tab_chrom.loc[tab_chrom['Chr'] == num_chr, :]['Chr ID'])[0]
            lengthID = list(tab_chrom.loc[tab_chrom['Chr'] == num_chr, :]['Length Chr'])[0]
            new_row = pd.Series(data={'Chr':num_chr, 'Chr ID': chrID, 'Length Chr':lengthID})
            table = table.append(new_row, ignore_index=True)

    return table
    
    
    
    
def main(TE_file, TFBS_file, overlapping_file):
        
    ##############################
    #### Retrieve result datas ###
    #### in tsv files          ###
    ##############################
    
    # About all TE
    tab_allTEs = pd.read_csv(TE_file, sep="\t")
    # About TEs which have overlapped
    tab_TEoverlap = pd.read_csv(overlapping_file, sep="\t")

    # TE name
    TEname = tab_TEoverlap['TE name'][0]
    # About Genes and GO
    tabGenesGO = pd.read_csv('../Results/Genic-Environment/genic-environment_{}.tsv'.format(TEname), sep="\t")
    
    # Retrieve length of chromosomes
    tab_chrom = pd.read_csv('../Data/humanGenome.txt', sep="\t")

    # Select only TE not overlapping
    tab_TEnotOverlap = pd.read_csv('../Results/ShorterDistances-GenesTEs/{}.tsv'.format(TEname), '\t')
    tab_TEnotOverlap = tab_TEnotOverlap.loc[tab_TEnotOverlap['Overlap TE/TFBS'] == 'No',:]

    # Merge framadates
    tab_allTEs = tab_allTEs.merge(tab_chrom)
    tab_TEoverlap = tab_TEoverlap.merge(tab_chrom)
    
    e = 0
    # Add 'chr' string
    while e < len(tabGenesGO):
        tabGenesGO['Chr'][e] = 'chr' + str(tabGenesGO['Chr'][e])
        e+=1
                
    tabGenesGO = tabGenesGO.merge(tab_chrom)
    
    # Include chromosomes without TE overlapping
    tab_allTEs = includeAbsentChr(tab_allTEs, tab_chrom)
    tab_TEoverlap = includeAbsentChr(tab_TEoverlap, tab_chrom)
        

    # For mitochondrion
    if 'MT' not in list(tab_allTEs['Chr']):
        mitochondrion_row = pd.Series(data={'Chr':'MT', 'Chr ID':'Mitochondrion', 'Length Chr':16569}) # add mitonchondrion informations
        tab_allTEs = tab_allTEs.append(mitochondrion_row, ignore_index=True)
    if 'MT' not in list(tab_TEoverlap['Chr']):
        mitochondrion_row = pd.Series(data={'Chr':'MT', 'Chr ID':'Mitochondrion', 'Length Chr':16569}) # add mitonchondrion informations
        tab_TEoverlap = tab_TEoverlap.append(mitochondrion_row, ignore_index=True)


    # Sort dataframes in ascending alphabetic-numeric order
    tab_allTEs = tab_allTEs.sort_values(by = 'Chr ID', ascending = False)
    tab_TEoverlap = tab_TEoverlap.sort_values(by = 'Chr ID', ascending = False)
    #tab_TEnotOverlap = tab_TEnotoverlap.sort_values(by = 'Chr ID', ascending = False)
    
    # Create folder
    if not os.path.exists('../Results/Graphics/'):
        os.mkdir('../Results/Graphics/')
    if not os.path.exists('../Results/Graphics/Overlaps'):
        os.mkdir('../Results/Graphics/Overlaps')
    if not os.path.exists('../Results/Graphics/proportionsTFBSoverlapping'):
        os.mkdir('../Results/Graphics/proportionsTFBSoverlapping')
    if not os.path.exists('../Results/Graphics/proportionsGO'):
        os.mkdir('../Results/Graphics/proportionsGO')
    
    ########################
    ### Output file name ###
    ########################
    pathOutputBarPlotOverlap = '../Results/Graphics/Overlaps/{}.html'.format(TEname)
    pathOutputTFBS = '../Results/Graphics/proportionsTFBSoverlapping/{}.html'.format(TEname)
    pathOutputGO = '../Results/Graphics/proportionsGO/{}.html'.format(TEname)
    
    
    ###########################################################
    ### Graphic representation of overlaps in all genome    ###
    ###                     Barplot                         ###
    ###########################################################

    # Plot overlaps in all genome
    barPlot_TEs(tab_TEoverlap, tab_TEnotOverlap, tabGenesGO, 'Start TE', 'End TE', 'Chr', 'Environment around TE having overlapped with a TFBS for {} TE family.'.format(TEname), pathOutputBarPlotOverlap)
        
    
    ###############################
    ### Bar plot of frequencies ###
    ###############################
    
    # all TFBS used during analysis
    list_of_TFBS = list_TFBS(TFBS_file)
    
    # Bar chart with all TFBS overlapping with this TE
    #barChart_TFBS(list_of_TFBS, tab_TEoverlap, pathOutputTFBS)
    title_of_graph_TFBS = "Proportion of TFBS overlapping with {} transposable element. Total TFBS overlapping occurences: {}. Total unique TFBS overlapping: {}."
    elementTFBS = 'Protein'
    
    # Remove occurences of TE with same chrosomosome, start end and same TFBS
    barChartFrequencies(list_of_TFBS, TEname, tab_TEoverlap, elementTFBS, title_of_graph_TFBS, pathOutputTFBS)
    
    #tabGenesGO2 = tabGenesGO2.drop_duplicates(subset=['Chr', 'Start TE', 'End TE', 'Protein'], keep=False)
    
    # Bar chart whit GO proportion
    title_of_graph_GO = "Proportion of Gene Ontology (GO) Functions around overlaps between {} transposable element and TFBS. Total GO Function found occurences: {}. Total unique of GO Function: {}."
    elementGO = 'Function GO'
    barChartFrequencies(list_of_TFBS, TEname, tabGenesGO, elementGO, title_of_graph_GO, pathOutputGO)



if __name__ == '__main__':
    arguments = parse_arguments()
    main(**arguments)