#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

# Overlapping between TE (transposable elements) coordinates and TFBS (Transcription factor binding site) coordinates



########################################################################################################################
###	Overlapping between a family TE and all TFBS (Transcription Factor Binding Site)
########################################################################################################################

def overlapping(TEname, dataFrame_MyTE, dataFrame_TFBS, nts_NecessaryInPeak):
    
    ''' Parameters:
        1. TEname: name of TE family
        2. dataFrame_MyTE: informations about TE that we are currently analyzing
        3. directory: folder where there is the file with the current TFBS data
        4. nts_NecessaryInPeak: number of nucleotides in peak which have overlapped TE neccessary
    '''
    # Retrieve name of this current TFBS
    TFBS_name = dataFrame_TFBS['TFBS_ENCFF'][0]    
    TFBS_ENCSR = dataFrame_TFBS['TFBS_ENCSR'][0]
    
	# list of data structure
    matchs = []
    
    len_dfTE = len(dataFrame_MyTE)
    id_TE = 0
    
    while id_TE < len_dfTE:
        
        # ABOUT TE
        # chromosome
        chr_ID = dataFrame_MyTE["Chr ID"][id_TE]
        chr_TE = dataFrame_MyTE["Chr"][id_TE]
        
        # start
        start_TE = dataFrame_MyTE["Start"][id_TE]
        start_TE = int(start_TE)
        
        # end
        end_TE = dataFrame_MyTE["End"][id_TE]
        end_TE = int(end_TE)
        
        # size
        size_TE = dataFrame_MyTE["Size"][id_TE]            
        size_TE = int(size_TE)
        
        # In dataframe of TFBS, select only part having same chromosome as TE
        masque = dataFrame_TFBS["Chr"] == chr_TE
        resultsTFBS = dataFrame_TFBS[masque]
                
        id_TFBS = 0
        while id_TFBS < len(resultsTFBS):
            
            # start
            start_TFBS = dataFrame_TFBS["Start"][id_TFBS]
            start_TFBS = int(start_TFBS)
            
            # end
            end_TFBS = dataFrame_TFBS["End"][id_TFBS]
            end_TFBS = int(end_TFBS)

            # size
            size_TFBS = dataFrame_TFBS["Size"][id_TFBS]            
            size_TFBS = int(size_TFBS)
            
            # middle coordinate of the peak
            middle_TFBS = end_TFBS - size_TFBS/2
            
            # coordinates of peak ChIP-seq (TFBS) according to % of peak which TE must at leat overlap this TFBS
            start_peak = middle_TFBS - nts_NecessaryInPeak
            end_peak = middle_TFBS + nts_NecessaryInPeak

            # A number of peak for which TE must at leat overlap this TFBS
            if (start_TE <= start_peak) and (end_TE >= end_peak):                
                
                # Calculate the alignment size
                start_alignment = max(start_TE, start_TFBS) # At the start of alignment, take element overlapping the other element further
                end_alignment = min(end_TE, end_TFBS) # At the end of alignment, take element which ends faster
                size_alignment = end_alignment - start_alignment
                
                # similarity between TE and this genome portion
                similarity = dataFrame_MyTE["Similarity"][id_TE]
                
                # protein used in this ChIP-seq
                protein = dataFrame_TFBS['Protein'][id_TFBS]
        
                # biosample used in this ChIP-seq
                biosample = dataFrame_TFBS['Biosample'][id_TFBS]
               
                # assign value to data structure
                list_a_match = [chr_TE, chr_ID, TEname, TFBS_name, TFBS_ENCSR, start_TE, end_TE, start_TFBS, end_TFBS, size_alignment, similarity, protein, biosample]
                matchs.append(list_a_match)
                
                print(list_a_match)
            
            id_TFBS+=1
        id_TE+=1
        
    # convert to panda format
    list_overlapping_all = [x for x in matchs if x != []]
    dataFrame_overlapping = pd.DataFrame(list_overlapping_all, columns=['Chr', 'Chr ID', 'TE name', 'TFBS ENCFF', 'TFBS ENCSR', 'Start TE', 'End TE', 'Start TFBS', 'End TFBS', 'Size of alignment', 'Similarity', 'Protein', 'Biosample'])
    
    return dataFrame_overlapping